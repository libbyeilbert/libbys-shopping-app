from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse


class ItemCategory(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(
        null=True,
        blank=False,
        unique=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_api_url(self):
        return reverse("api_show_item_category", kwargs={"slug": self.slug})


class Source(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(
        null=True,
        blank=False,
        unique=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_api_url(self):
        return reverse("api_show_source", kwargs={"slug": self.slug})


class RecipeCategory(models.Model):
    name = models.CharField(max_length=50),
    slug = models.SlugField(
        null=True,
        blank=False,
        unique=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_api_url(self):
        return reverse("api_show_recipe_category", kwargs={"slug": self.slug})


class Recipe(models.Model):
    name = models.CharField(max_length=150)
    category = models.ForeignKey(
        RecipeCategory,
        related_name="recipes",
        on_delete=models.PROTECT,
    )
    ingredients = models.ManyToManyField(
        'Item',
        blank=True
    )
    slug = models.SlugField(
        null=True,
        blank=False,
        unique=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_api_url(self):
        return reverse("api_show_recipe", kwargs={"slug": self.slug})


class Item(models.Model):
    name = models.CharField(max_length=100)
    in_stock = models.BooleanField(default=True)
    category = models.ForeignKey(
        ItemCategory,
        related_name="items",
        on_delete=models.PROTECT,
    )
    source = models.ForeignKey(
        Source,
        related_name="items",
        on_delete=models.PROTECT,
    )
    recipes = models.ManyToManyField(
        Recipe,
        blank=True
    )
    slug = models.SlugField(
        null=True,
        blank=False,
        unique=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_api_url(self):
        return reverse("api_show_item", kwargs={"slug": self.slug})
