from shopping_list.models import Recipe, RecipeCategory, Item, ItemCategory, Source
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class RecipeCategoryListEncoder(ModelEncoder):
    model = RecipeCategory
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_recipe_categories(request):
    if request.method == "GET":
        categories = RecipeCategory.objects.all()
        return JsonResponse(
            {"categories": categories},
            encoder=RecipeCategoryListEncoder,
        )
    else:
        content = json.loads(request.body)
        category = RecipeCategory.objects.create(**content)
        return JsonResponse(
            category,
            encoder=RecipeCategoryDetailEncoder,
            safe=False
        )


class RecipeCategoryDetailEncoder(ModelEncoder):
    model = RecipeCategory
    properties = [
        "name",
        "slug",
    ]


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_recipe_category(request, slug):
    if request.method == "GET":
        category = RecipeCategory.objects.get(slug=slug)
        return JsonResponse(
            {"category": category},
            encoder=RecipeCategoryDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = RecipeCategory.objects.filter(slug=slug).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        RecipeCategory.objects.filter(slug=slug).update(**content)
        category = RecipeCategory.objects.get(slug=slug)
        return JsonResponse(
            category,
            encoder=RecipeCategoryDetailEncoder,
            safe=False
        )


class SourceListEncoder(ModelEncoder):
    model = Source
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_sources(request):
    if request.method == "GET":
        sources = Source.objects.all()
        return JsonResponse(
            {"sources": sources},
            encoder=SourceListEncoder,
        )
    else:
        content = json.loads(request.body)
        source = Source.objects.create(**content)
        return JsonResponse(
            source,
            encoder=SourceDetailEncoder,
            safe=False,
        )


class SourceDetailEncoder(ModelEncoder):
    model = Source
    properties = [
        "name",
        "slug"
    ]


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_source(request, slug):
    if request.method == "GET":
        source = Source.objects.get(slug=slug)
        return JsonResponse(
            {"source": source},
            encoder=SourceDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count, _ = Source.objects.filter(slug=slug).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Source.objects.filter(slug=slug).update(**content)
        source = Source.objects.get(slug=slug)
        return JsonResponse(
            source,
            encoder=SourceDetailEncoder,
            safe=False
        )


class ItemCategoryListEncoder(ModelEncoder):
    model = ItemCategory
    properties = [
        "name",
    ]


@require_http_methods("GET", "POST")
def api_list_item_categories(request):
    if request.method == "GET":
        categories = ItemCategory.objects.all()
        return JsonResponse(
            {"categories": categories},
            encoder=ItemCategoryListEncoder,
        )
    else:
        content = json.loads(request.body)
        category = ItemCategory.objcts.create(**content)
        return JsonResponse(
            category,
            encoder=ItemCategoryDetailEncoder,
            safe=False
        )


class ItemCategoryDetailEncoder(ModelEncoder):
    model = ItemCategory
    properties = [
        "name",
        "slug",
    ]


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_item_category(request, slug):
    if request.method == "GET":
        category = ItemCategory.objects.get(slug=slug)
        return JsonResponse(
            {"category": category},
            encoder=ItemCategoryDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _= ItemCategory.objects.filter(slug=slug).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        ItemCategory.objects.filter(slug=slug).update(**content)
        category = ItemCategory.objects.get(slug=slgug)
        return JsonResponse(
            category,
            encoder=ItemCategoryDetailEncoder,
            safe=False
        )


class RecipeListEncoder(ModelEncoder):
    model = Recipe
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_recipes(request):
    if request.method == "GET":
        recipes = Recipe.objects.all()
        return JsonResponse(
            {"recipes": recipes},
            encoder=RecipeListEncoder,
        )
    else:
        content = json.loads(request.body)
        recipe = Recipe.objects.create(**content)
        return JsonResponse(
            recipe,
            encoder=RecipeDetailEncoder,
            safe=False,
        )


class ItemListEncoder(ModelEncoder):
    model = Item
    properties = [
        "name",
    ]


class RecipeDetailEncoder(ModelEncoder):
    model = Recipe
    properties = [
        "name",
        "category",
        "ingredients",
        "slug",
    ]
    encoders = {
        "category": RecipeCategoryListEncoder(),
        "ingredients": ItemListEncoder(),
    }


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_recipe(request, slug):
    if request.method == "GET":
        recipe = Recipe.objects.get(slug=slug)
        return JsonResponse(
            {"recipe": recipe},
            encoder=RecipeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Recipe.objects.filter(slug=slug).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Recipe.objects.filter(slug=slug).update(**content)
        recipe = Recipe.objects.get(slug=slug)
        return JsonResponse(
            recipe,
            encoder=RecipeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_items(request):
    if request.method == "GET":
        items = Item.objects.all()
        return JsonResponse(
            {"items": items},
            encoder=ItemListEncoder,
        )
    else:
        content = json.loads(request.body)
        item = Item.objets.create(**content)
        return JsonResponse(
            item,
            encoder=ItemDetailEncoder,
            safe=False,
        )


class ItemDetailEncoder(ModelEncoder):
    model = Item
    properties = [
        "name",
        "in_stock",
        "category",
        "source",
        "recipes",
        "slug",
    ]
    encoders = {
        "category": ItemCategoryListEncoder(),
        "source": SourceListEncoder(),
        "recipes": RecipeListEncoder(),
    }


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_item(request, slug):
    if request.method == "GET":
        item = Item.objects.get(slug=slug)
        return JsonResponse(
            {"item": item},
            encoder=ItemDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Item.objects.filter(slug=slug).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.laods(request.body)
        Item.objects.filter(slug=slug).update(**content)
        item = Item.objects.get(slug=slug)
        return JsonResponse(
            item,
            encoder=ItemDetailEncoder,
            safe=False,
        )
