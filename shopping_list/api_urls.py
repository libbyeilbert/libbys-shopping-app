from django.urls import path
from shopping_list.api_views import (
    api_list_item_categories,
    api_list_items,
    api_list_recipe_categories,
    api_list_recipes,
    api_list_sources,
    api_show_item,
    api_show_item_category,
    api_show_recipe,
    api_show_recipe_category,
    api_show_source
)

urlpatterns = [
    path("items/categories/", api_list_item_categories, name="api_list_item_categories"),
    path("items/", api_list_items, name="api_list_items"),
    path("recipes/categories/", api_list_recipe_categories, name="api_list_recipe_categories"),
    path("recipes/", api_list_recipes, name="api_list_recipes"),
    path("sources/", api_list_sources, name="api_list_sources"),
    path("items/<slug:slug>/", api_show_item, name="api_show_item"),
    path("items/categories/<slug:slug>/", api_show_item_category, name="api_show_item_category"),
    path("recipes/<slug:slug>/", api_show_recipe, name="api_show_recipe"),
    path("recipes/categories/<slug:slug>/", api_show_recipe_category, name="api_show_recipe_category"),
    path("source/<slug:slug>/", api_show_source, name="api_show_source")
]
