from django.contrib import admin
from shopping_list.models import Recipe, RecipeCategory, Item, ItemCategory, Source

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "category",
        "slug",
        "id"
    )

@admin.register(RecipeCategory)
class RecipeCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "slug",
        "id"
    )

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "in_stock",
        "category",
        "source",
        "slug",
        "id"
    )


@admin.register(ItemCategory)
class ItemCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "slug",
        "id"
    )


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "slug",
        "id"
    )
